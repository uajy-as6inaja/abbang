import React from 'react';
import { LoadScript } from "@react-google-maps/api";
import { Provider } from "mobx-react";
import Userstore from "~/store/userstore";
import Indexer from "./components/indexer/Indexer";

function App() {
  return (
    <div>
      <LoadScript
        googleMapsApiKey={process.env.REACT_APP_GOOGLE_MAP_KEY}
        libraries={["visualization"]}
      >
        <Provider userstore={new Userstore()}>
          <Indexer />
        </Provider>
      </LoadScript>
    </div>
  );
}

export default App;
