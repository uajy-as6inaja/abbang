/* global google */
import { computed, action, observable, decorate } from "mobx";
import axios from 'axios';

class UserStore {
    heatmap_peta = []
    get heatmap_data() {
        let datas = this.heatmap_peta;
        if(this.animasi_enabled) {
            datas = this.heatmap_peta.filter((e)=>e.created_on === this.tanggals[this.leap])
        }
        return datas.map((e)=>{
            return {
                location:new google.maps.LatLng(e.latitude, e.longitude),
                weight: 3
            };
        })
    }
    
    leap = 0;


    get tanggals() {
        let tanggals = new Set();
        this.heatmap_peta.map((barang)=>{
            return tanggals.add(barang.created_on);
        })

        return [...tanggals].sort();
    }

    animasi_enabled = false;

    centermap = {lat:-7.779160, lng:110.414759}

    genRandomData() {
        this.heatmap_peta = [
            ...this.randomize(this.centermap, 0.06, 50),
            // ...this.randomize(this.movepos(this.centermap, -0.06, 0.08), 0.06, 300),
            // ...this.randomize(this.movepos(this.centermap, 0.00, 0.05), 0.05, 400),
            // ...this.randomize(this.movepos(this.centermap, 0.002, -0.08), 0.02, 200),
            // ...this.randomize(this.centermap, 0.13, 700),
        ];
    }

    fetch_data() {
        axios.get("/api/qwikend/pemeriksaan").then((e)=>{
            this.heatmap_peta = e.data.data;
        })
    }

    movepos(pos, dist_lat, dist_lng) {
        return {
            lat: pos.lat + dist_lat,
            lng: pos.lng + dist_lng
        }
    }

    randomize(pos, rad, n) {
        let kambing = [];

        while(n-->0) {
            let testing = {
                lat: Math.floor(rad*Math.random()*10e6)/10e6 ,
                lng: Math.floor(rad*Math.random()*10e6)/10e6
            }
            if(Math.floor(Math.random()*10)%3 === 0) {
                testing.lat *= -1;
            }
            if(Math.floor(Math.random()*10)%3 === 0) {
                testing.lng *= -1;
            }
            let kambong = {
                lat: testing.lat + pos.lat,
                lng: testing.lng + pos.lng
            }; 
            if(this.mustInDistance(pos,kambong, rad)) {
                kambing.push(kambong);
            }
        }

        return kambing;
    }

    mustInDistance(pos_a, pos_b, radius) {
        let eucledian = Math.sqrt(Math.pow(pos_a.lat-pos_b.lat, 2) + Math.pow(pos_a.lng-pos_b.lng, 2));
        return radius >= eucledian;
    }

    openFilterModal = false

    listGejala=[]
    listKlasifikasi=[]

    fetchGejala() {
        axios.get("/api/qwikend/gejala").then((e)=>{
            this.listGejala = e.data.data;
            return axios.get("/api/qwikend/klasifikasi").then((e)=>{
                this.listKlasifikasi = e.data.data;
                return Promise.resolve();
            })
        })
    }

    centroids=[]

    fetchCentroids() {
        axios.get("/api/qwikend/centroid").then((e)=>{
            this.centroids = e.data.data;
        });
    }
}

decorate(UserStore, {
    heatmap_peta: observable,
    heatmap_data: computed,
    genRandomData: action,
    centermap: observable,
    fetch_data: action,
    fetchGejala:action,
    listKlasifikasi:observable,
    listGejala:observable,
    openFilterModal:observable,
    leap:observable,
    tanggals: computed,
    animasi_enabled: observable
})

export default UserStore;