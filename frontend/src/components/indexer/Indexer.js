/* global google */
import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { GoogleMap, HeatmapLayer, Circle } from "@react-google-maps/api";
import { gmap_style } from 'config';
import Filterizer from 'components/filterizer/Filterizer';
import FilterModal from "components/FilterModal/FilterModal";
import Timeliner from 'components/timeliner/Timeliner';
import { When } from 'react-if';
class Indexer extends Component {
  onMapLoaded(map) {
    const { userstore } = this.props;
    userstore.fetch_data();
    userstore.fetchGejala();
    userstore.fetchCentroids();
  }
  render() {
    const { userstore } = this.props;
    return (
      <div className="Indexer">
        <GoogleMap
          mapContainerStyle={{
            height: "100vh",
            width: "100vw"
          }}
          onLoad={this.onMapLoaded.bind(this)}
          center={userstore.centermap}
          zoom={13}
          options={{
            disableDefaultUI: true,
            fullscreenControl: false,
            streetViewControl: false,
            ControlPosition: 'TOP_RIGHT',
            styles: gmap_style
          }}>
          <HeatmapLayer data={userstore.heatmap_data} options={{
            dissipating: true,
            maxIntensity: 10,
            radius: 10,
          }} />
          {userstore.centroids.map((e) =>
            <Circle key={"kambing-" + e._id} center={new google.maps.LatLng(e.latitude, e.longitude)} radius={3000} options={{
              strokeColor: '#ffffff',
              strokeOpacity: 0.4,
              strokeWeight: 2,
              fillColor: '#ffffff',
              fillOpacity: 0.3,
              clickable: false,
              draggable: false,
              editable: false,
              visible: true,
              zIndex: 1
            }} />
          )}

        </GoogleMap>
        {/* Bagian controllernya */}
        <FilterModal />
        <Filterizer />
        <When condition={userstore.animasi_enabled}>
          <Timeliner />
        </When>
      </div>
    )
  }
}
Indexer.propTypes = {
}

export default inject('userstore')(observer(Indexer));