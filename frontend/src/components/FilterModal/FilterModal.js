import React from 'react';
import { inject, observer } from 'mobx-react';

import Modal from 'reactstrap/lib/Modal';
import ModalBody from 'reactstrap/lib/ModalBody';
import ModalFooter from 'reactstrap/lib/ModalFooter';
import FormGroup from 'reactstrap/lib/FormGroup';
import Label from 'reactstrap/lib/Label';
import Input from 'reactstrap/lib/Input';
import Form from 'reactstrap/lib/Form';
import Row from 'reactstrap/lib/Row';
import Col from 'reactstrap/lib/Col';
import Button from 'reactstrap/lib/Button';
import Axios from 'axios';

const FilterModal = ({ userstore }) => {
  const cboGejala = React.createRef();
  const cboKlasifikasi = React.createRef();
  return (
    <div className="FilterModal">
      <Modal isOpen={userstore.openFilterModal} toggle={() => userstore.openFilterModal = !userstore.openFilterModal}>
        <ModalBody>
          <Form>
            <Row>
              <Col>
                <FormGroup>
                  <Label for="exampleSelect">Gejala</Label>
                  <Input type="select" name="select" id="gejala" innerRef={cboGejala}>
                    <option value="">(none)</option>
                    {userstore.listGejala.map((e) => <option  key={"ge-" + e._id} value={e._id}>{e.full_string}</option>)}
                  </Input>
                </FormGroup>
              </Col>
              <Col>
                <FormGroup>
                  <Label for="exampleSelect">Klasifikasi</Label>
                  <Input type="select" name="select" id="klasifikasi" innerRef={cboKlasifikasi}>
                    <option value="">(none)</option>
                    {userstore.listKlasifikasi.map((e) => <option key={"kl-" + e._id} value={e._id}>{e.full_string}</option>)}
                  </Input>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col>
                <FormGroup check>
                  <Label check>
                    <input type="checkbox" onChange={(e)=>{
                      userstore.animasi_enabled = e.target.checked;
                    }} value="animasi" name="animation_active" checked={userstore.animasi_enabled}/>{' '}
                    Gunakan Animasi
                  </Label>
                </FormGroup>
              </Col>
            </Row>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button color="success" onClick={(e)=>{
            Axios.get("/api/qwikend/pemeriksaan", {
              params:{
                gejala:cboGejala.current.value,
                klasifikasi:cboKlasifikasi.current.value
              }
            }).then((e)=>{
              userstore.heatmap_peta = e.data.data;
              userstore.openFilterModal = false;
            })
          }}>Filter</Button>
        </ModalFooter>
      </Modal>
    </div>
  )
}
FilterModal.propTypes = {
}

export default inject('userstore')(observer(FilterModal));