import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import Card from "reactstrap/lib/Card";
import CardBody from "reactstrap/lib/CardBody";
import Container from "reactstrap/lib/Container";
import Col from "reactstrap/lib/Col";
import Row from "reactstrap/lib/Row";
import Button from "reactstrap/lib/Button";
class Filterizer extends Component {
  bukaFilter(){
    const { userstore } = this.props;
    userstore.openFilterModal = true;
  }
  render() {
    return (
      <Container fluid className="fixed-top px-0">
        <Col sm={12} md={4} className="px-0">
          <div className="p-3">
            <Card color="success">
              <CardBody className="text-light p-3">
                <Row className="align-items-center">
                  <Col xs={6}>
                    Cari Daerah
                  </Col>
                  <Col xs={6}>
                    <div className="text-right">
                      <Button color="light" outline onClick={this.bukaFilter.bind(this)} size="sm">Filter</Button>
                    </div>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </div>
        </Col>
      </Container>
    )
  }
}
Filterizer.propTypes = {
}

export default inject('userstore')(observer(Filterizer));