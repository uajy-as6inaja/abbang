import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import Row from 'reactstrap/lib/Row'
import Container from 'reactstrap/lib/Container'
import Col from 'reactstrap/lib/Col'
import Card from 'reactstrap/lib/Card'
import CardBody from 'reactstrap/lib/CardBody'
import Input from 'reactstrap/lib/Input'
import FormGroup from 'reactstrap/lib/FormGroup'
import Label from 'reactstrap/lib/Label'

class Timeliner extends Component {
  intervaler = null;
  componentDidMount() {
    const {userstore} = this.props;
    this.intervaler = setInterval(()=>{
      userstore.leap = (userstore.leap + 1) % userstore.tanggals.length
    }, 3000)
  }
  componentWillUnmount() {
    clearInterval(this.intervaler);
  }
  render() {
    const {userstore} = this.props;

    return (
      <Container className="fixed-bottom" fluid>
        <Row>
          <Col className="p-4">
            <Card>
              <CardBody className="p-3">
                <FormGroup className="p-0">
                  <Label> Tanggal Saat ini: <b>{userstore.tanggals[userstore.leap]}</b></Label>
                  <Input type="range" min={0} max={userstore.tanggals.length-1} defaultValue={userstore.leap}/>
                </FormGroup>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    )
  }
}
Timeliner.propTypes = {
}

export default inject('userstore')(observer(Timeliner));