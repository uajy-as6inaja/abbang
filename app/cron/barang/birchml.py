#!/usr/bin/env python3
import pandas as pd 
from sklearn.cluster import Birch
import sys

data_input = sys.argv[1]
data_output = sys.argv[2]
data_centroids = sys.argv[3]
dataframe = pd.read_csv(data_input, sep=',', header=0)

pred = Birch(n_clusters=None, threshold=0.045, branching_factor=10000)
filtered = dataframe[["lat", "lng"]]
pred.fit(filtered)
clusters = pred.predict(filtered)

combined = dataframe.assign(cluster=clusters)
centroids = pd.DataFrame(pred.subcluster_centers_)

combined.to_csv(data_output,  sep=',', header=0)
centroids.to_csv(data_centroids, sep=',', header=0)
