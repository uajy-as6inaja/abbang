<?php
namespace Cron;

class DoML
{
    public function boot($f3)
    {
        set_time_limit(0);
        $barang = __DIR__ . "/barang/birchml.py";
        $temp_input = tempnam($f3->TEMP, "birch");
        $temp_cluster = tempnam($f3->TEMP, "clusters");
        $temp_centroids = tempnam($f3->TEMP, "birch");

        echo "writing things to Csv for our ml to proccess...\n";
        //write all things to csv.
        $handler = fopen($temp_input, 'w+');
        $pemeriksaan = new \Model\Pemeriksaan();
        $ps = $pemeriksaan->find(['1']);
        fputcsv($handler, ["id", "lat", "lng"]);

        foreach ($ps as $p) {
            fputcsv($handler, [$p->_id, $p->latitude, $p->longitude]);
        }
        \fclose($handler);
        echo "Finished writing.\n\n";

        echo "Starting execution for python3...\n";
        // PROCCESS
        $hasil = \shell_exec(implode(" ", ["python3", $barang, $temp_input, $temp_cluster, $temp_centroids]));
        echo "Finished.\n\n";

        //insert centroids:
        $centro = new \Model\Centroid();
        $centro->self_truncate();

        //importing centroid
        echo "Inserting centroids...\n";
        $sendro = array_map('str_getcsv', file($temp_centroids));
        $cluster_id=[];
        foreach($sendro as $c) {
            $centro->reset();
            $centro->copyfrom([
                "latitude"=>$c[1],
                "longitude"=>$c[2],
                "cluster_id"=>$c[0]
            ]);
            $centro->save();
            $cluster_id[$c[0]] = $centro->_id;
            echo "inserting " . $c[0] . " as " . $centro->_id . "\n";
        }
        echo "Finished.\n\n";


        echo "Inserting it to pemeriksaan...\n";
        $handler = fopen($temp_cluster, "r");
        while (($line = fgetcsv($handler)) !== FALSE) {
            echo implode(" | ", $line) . "\n";
            $pemeriksaan = new \Model\Pemeriksaan();
            $pemeriksaan->load(['id=?', $line[1]]);
            $pemeriksaan->cluster = $cluster_id[$line[4]];
            $pemeriksaan->save();
            echo "updated " . $pemeriksaan->_id . " --- " . $cluster_id[$line[4]] . " (" . $line[4] ." ).\n";
        }
        fclose($handler);
        echo "Finished.\n\n";

        echo "Cleaning ups...\n";
        //cleanups
        \unlink($temp_centroids);
        \unlink($temp_cluster);
        \unlink($temp_input);
        echo "Done.\n\n";
    }
}
