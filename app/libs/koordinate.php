<?php
Namespace Libs;

class Koordinate extends \Prefab {
    public function generate($start, $rad) {

        $par = [];
        while(!$par || ($this->eucledian($start, $par) > $rad)) {
            $par = [
                "lat" => round(rand(-20e6, 20e6)/10e6 * $rad + $start['lat'], 6) ,
                "lng" => round(rand(-20e6, 20e6)/10e6 * $rad + $start['lng'], 6) 
            ];
        }

        return $par;
    }

    public function generate_n($start, $rad, $num) {
        $vals = [];
        while($num-->0) {
            $vals[] = $this->generate($start, $rad);
        }

        return $vals;
    }

    public function eucledian($point_a, $point_b) {
        return sqrt(pow($point_a['lat'] - $point_b['lat'], 2) + pow($point_a['lng'] - $point_b['lng'], 2));
    }
}