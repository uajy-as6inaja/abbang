<?php
namespace Model;

class Klasifikasi extends \DB\Cortex {

  protected
    $fieldConf = array(
        'kode' => array(
            'type' => \DB\SQL\Schema::DT_TEXT
        ),
        'full_string' => array(
            'type' =>  \DB\SQL\Schema::DT_TEXT
        ),
        "pemeriksaan" => [
            "has-many"=> ["\\Model\\Pemeriksaan", "klasifikasi", "pemeriksaan_klasifikasi"]
        ],
    ),
    $db = 'DB',
    $table = 'klasifikasi';
}