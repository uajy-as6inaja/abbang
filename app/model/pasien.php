<?php
namespace Model;

class Pasien extends \DB\Cortex {

  protected
    $fieldConf = array(
        'name' => array(
            'type' => \DB\SQL\Schema::DT_TEXT
        ),
        'nama_ibu' => array(
            'type' =>  \DB\SQL\Schema::DT_TEXT
        ),
        "pemeriksaan" => [
            'has-many' => ["\\Model\\Pemeriksaan", "pasien"]
        ]
    ),
    $db = 'DB',
    $table = 'pasien';
}