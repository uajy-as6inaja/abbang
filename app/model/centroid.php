<?php
namespace Model;

class Centroid extends \DB\Cortex {

  protected
    $fieldConf = array(
        'latitude' => array(
            'type' => \DB\SQL\Schema::DT_TEXT
        ),
        'longitude' => array(
            'type' =>  \DB\SQL\Schema::DT_TEXT
        ),
        "pemeriksaan" => [
            "has-many"=> ["\\Model\\Pemeriksaan", "cluster"]
        ]
    ),
    $db = 'DB',
    $table = 'sentroid';

    public function self_truncate(){
        \Base::instance()->DB->exec('TRUNCATE TABLE ' . $this->table);
    }
}