<?php
namespace Model;

class Pemeriksaan extends \DB\Cortex {

  protected
    $fieldConf = array(
        'latitude' => array(
            'type' => \DB\SQL\Schema::DT_TEXT
        ),
        'longitude' => array(
            'type' =>  \DB\SQL\Schema::DT_TEXT
        ),
        'created_on' => array(
            'type' =>  \DB\SQL\Schema::DT_DATE
        ),
        "klasifikasi" => [
            "has-many"=> ["\\Model\\Klasifikasi", "pemeriksaan", "pemeriksaan_klasifikasi"]
        ],
        "gejala" => [
            "has-many"=> ["\\Model\\Gejala", "pemeriksaan", "pemeriksaan_gejala"]
        ],
        "pasien" => [
            "belongs-to-one"=> "\\Model\\Pasien"
        ],
        "cluster" => [
            "belongs-to-one"=> "\\Model\\Centroid"
        ]
    ),
    $db = 'DB',
    $table = 'pemeriksaan';



    public function cast($obj = NULL, $rel_depths = 1, bool $safe_mode = true)
    {
        $objx = parent::cast($obj, $rel_depths);
        if($safe_mode) {
            unset($objx['pasien']);
        }
        return $objx;
    }

    public function set_created_on($date) {
        return date("Y-m-d", $date);
    }

    public function save() {
        if(!$this->created_on) {
            $this->created_on = time();
        }
        
        parent::save();
    }
}