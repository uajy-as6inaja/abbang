<?php
namespace Model;

class Topik extends \DB\Cortex {

  protected
    $fieldConf = array(
        'kode' => array(
            'type' => \DB\SQL\Schema::DT_TEXT
        ),
        'full_string' => array(
            'type' =>  \DB\SQL\Schema::DT_TEXT
        ),
        "gejala" => [
            "has-many"=> ["\\Model\\Gejala", "topik"]
        ]
    ),
    $db = 'DB',
    $table = 'topik';
}