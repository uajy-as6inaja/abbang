<?php
namespace Model;

class Gejala extends \DB\Cortex {

  protected
    $fieldConf = array(
        'kode' => array(
            'type' => \DB\SQL\Schema::DT_TEXT
        ),
        'full_string' => array(
            'type' =>  \DB\SQL\Schema::DT_TEXT
        ),
        "pemeriksaan" => [
            "has-many"=> ["\\Model\\Pemeriksaan", "gejala", "pemeriksaan_gejala"]
        ],
        "topik" => [
            "belongs-to-one" => "\\Model\\Topik"
        ]
    ),
    $db = 'DB',
    $table = 'gejala';
}