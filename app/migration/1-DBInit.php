<?php
namespace Migration;
/**
 * Migration Example
 * Please read more documentation on https://github.com/chez14/f3-ilgar
 */
class DBInit extends \Chez14\Ilgar\MigrationPacket {
    public function on_migrate(){
        // Do your things here!
        // All the F3 object were loaded, F3 routines executed,
        // this will just like you doing things in your controller file.
        
        $f3 = \F3::instance(); //get the $f3 from here.
        
        \Model\Pasien::setup();
        \Model\Gejala::setup();
        \Model\Klasifikasi::setup();
        \Model\Pemeriksaan::setup();
    }

    public function on_failed(\Exception $e) {

    }
}