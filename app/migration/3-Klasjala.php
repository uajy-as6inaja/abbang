<?php
namespace Migration;
/**
 * Migration Example
 * Please read more documentation on https://github.com/chez14/f3-ilgar
 */
class Klasjala extends \Chez14\Ilgar\MigrationPacket {
    public function on_migrate(){
        // Do your things here!
        // All the F3 object were loaded, F3 routines executed,
        // this will just like you doing things in your controller file.
        
        $f3 = \F3::instance(); //get the $f3 from here.
        \Model\Gejala::setup();
        \Model\Topik::setup();

        // IMPORT STUFFS
        $gejala = array_map('str_getcsv', file(__DIR__ . '/3_data_csv/an_gejala.csv'));
        $topik = new \Model\Topik();
        $i = 0;
        foreach ($gejala as $g) {
            if($topik->dry() || $topik->kode != $g[2]) {
                $topik->load(['kode=?', $g[2]]);
                if($topik->dry()) {
                    $topik->copyfrom([
                        "kode" => $g[2],
                        "full_string" => $g[2]
                    ]);
                    $topik->save();
                }
            }

            $x = new \Model\Gejala();
            $x->copyfrom([
                "kode" => $g[1],
                "full_string" => $g[0],
                "topik" => $topik->_id
            ]);
            $x->save();
            $i++;
        }

        $klasifikasi = array_map('str_getcsv', file(__DIR__ . '/3_data_csv/an_klasifikasi.csv'));
        $cls = 0;
        foreach($klasifikasi as $k) {
            $kl = new \Model\Klasifikasi();
            $kl->copyfrom([
                'kode' =>  $k[0],
                'full_string' => $k[1]
            ]);
            $kl->save();
            $cls++;
        }

        echo "Successfully imported $i Gejala, and $cls Klasifikasi.\n\n";
    }

    public function on_failed(\Exception $e) {

    }
}