<?php
namespace Migration;

use Model\Pemeriksaan;

/**
 * Migration Example
 * Please read more documentation on https://github.com/chez14/f3-ilgar
 */
class SwarmData extends \Chez14\Ilgar\MigrationPacket
{
    public function on_migrate()
    {
        set_time_limit(0);
        // Do your things here!
        // All the F3 object were loaded, F3 routines executed,
        // this will just like you doing things in your controller file.

        $f3 = \F3::instance(); //get the $f3 from here.
        $swarms = [
            [
                "lat" => -7.779160 - 0.04,
                "lng" => 110.414759 - 0.08,
                "radius" => 0.03,
                "n" => [50, 200, 400, 500, 450, 430]
            ],
            [
                "lat" => -7.779160 + 0.02,
                "lng" => 110.414759 + 0.002,
                "radius" => 0.02,
                "n" => [100, 70, 150, 50, 60, 56]
            ],
            [
                "lat" => -7.779160 + 0.02,
                "lng" => 110.414759 + 0.002,
                "radius" => 0.02,
                "n" => [100, 70, 150, 50, 60, 56]
            ],
            [
                "lat" => -7.779160 + -0.06,
                "lng" => 110.414759 + 0.08,
                "radius" => 0.06,
                "n" => [150, 200, 230, 250, 400, 500]
            ],
            //noise:
            [
                "lat" => -7.779160,
                "lng" => 110.414759,
                "radius" => 0.06,
                "n" => [50, 70, 70, 50, 40, 50]
            ],
        ];

        //generating data:
        $datas = [];
        foreach ($swarms as $s) {
            $date = strtotime("1 April 2019");
            foreach ($s['n'] as $n) {
                $max_date = strtotime("+5 days", $date);
                $datas = array_merge($datas, array_map(
                    function ($data) use (&$date, &$max_date) {
                        $objek = [
                            "latitude" => $data['lat'],
                            "longitude" => $data['lng'],
                            "created_on" => rand($date, $max_date)
                        ];
                        return $objek;
                    },
                    \Libs\Koordinate::instance()->generate_n($s, $s['radius'], $n)
                ));

                $date = $max_date;
            }
        }

        array_map(function($dat){
            $pemeriksaan = new Pemeriksaan();
            $pemeriksaan->copyfrom($dat);
            $pemeriksaan->save();
        }, $datas);
    }

    public function on_failed(\Exception $e)
    { }
}
