<?php
namespace Migration;
/**
 * Migration Example
 * Please read more documentation on https://github.com/chez14/f3-ilgar
 */
class GejalaKlasifikasi extends \Chez14\Ilgar\MigrationPacket {
    public function on_migrate(){
        // Do your things here!
        // All the F3 object were loaded, F3 routines executed,
        // this will just like you doing things in your controller file.
        set_time_limit(0);
        $f3 = \F3::instance(); //get the $f3 from here.

        $penyakits = (array)(new \Model\Klasifikasi())->find(['1']);
        $gejalas = (array)(new \Model\Gejala())->find(['1']);
        $id_swarmer = (array)(new \Model\Pemeriksaan())->find(['1']);

        foreach($id_swarmer as $q) {
            $penyakit = array_map(function($id) use (&$penyakits){
                return $penyakits[$id];
            }, array_rand($penyakits, rand(2,3)));

            $gejala = array_map(function($id) use(&$gejalas) {
                return $gejalas[$id];
            }, array_rand($gejalas, rand(3,5)));
            
            $q->klasifikasi = $penyakit;
            $q->gejala = $gejala;
            $q->save();
        }
    }

    public function on_failed(\Exception $e) {

    }
}