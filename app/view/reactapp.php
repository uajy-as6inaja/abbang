<?php
namespace View;

class ReactApp {
    public static function render() {
        header("Content-type: text/html");
        echo file_get_contents(\F3::get('CZ_PUBLIC_HTML') . "index2.html");
    }
}
