<?php
namespace Controller\Api;

class Qwikend {
    public function get_pemeriksaan($f3){
        $pemeriksaan = new \Model\Pemeriksaan();
        if($f3->GET['klasifikasi']) {
            $pemeriksaan->has('klasifikasi', ["id=?", $f3->GET['klasifikasi']]);
        }
        if($f3->GET['gejala']) {
            $pemeriksaan->has('gejala', ["id=?", $f3->GET['gejala']]);
        }
        return \View\Api::success($pemeriksaan->find(['1'])->castAll(0));
    }

    public function get_klasifikasi($f3){
        $klasifikasi = new \Model\Klasifikasi();
        return \View\Api::success($klasifikasi->find(['1'])->castAll(0));
    }

    public function get_gejala($f3){
        $gejala = new \Model\Gejala();
        return \View\Api::success($gejala->find(['1'])->castAll(0));
    }
    public function get_centroid($f3){
        $gejala = new \Model\Centroid();
        return \View\Api::success(array_map(function($data){
            $data['longitude'] = round($data['longitude'], 6);
            $data['latitude'] = round($data['latitude'], 6);
            return $data;
        }, $gejala->find(['1'])->castAll(0)));
    }
}