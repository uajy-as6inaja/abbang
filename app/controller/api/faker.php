<?php

namespace Controller\Api;

class Faker
{
    public function post_fake_it($f3)
    {
        // generate berdasarkan tempat
        // swarm pertama:
        $swarms = [
            [
                "lat" => -7.779160 - 0.04,
                "lng" => 110.414759 - 0.08,
                "radius" => 0.03,
                "n" => [50, 200, 400, 500, 450, 430]
            ],
            [
                "lat" => -7.779160 + 0.02,
                "lng" => 110.414759 + 0.002,
                "radius" => 0.02,
                "n" => [100, 70, 150, 50, 60, 56]
            ],
            [
                "lat" => -7.779160 + 0.02,
                "lng" => 110.414759 + 0.002,
                "radius" => 0.02,
                "n" => [100, 70, 150, 50, 60, 56]
            ],
            [
                "lat" => -7.779160 + -0.06,
                "lng" => 110.414759 + 0.08,
                "radius" => 0.06,
                "n" => [150, 200, 230, 250, 400, 500]
            ],
            //noise:
            [
                "lat" => -7.779160,
                "lng" => 110.414759,
                "radius" => 0.06,
                "n" => [50, 70, 70, 50, 40, 50]
            ],
        ];

        //generating data:
        $datas = [];
        foreach ($swarms as $s) {

            $date = strtotime("1 April 2019");
            foreach ($s['n'] as $n) {
                $max_date = strtotime("+5 days", $date);
                $datas = array_merge($datas, array_map(
                    function ($data) use (&$date, &$max_date) {
                        // $joglo = new \Model\Pemeriksaan();
                        $objek = [
                            "lat" => $data['lat'],
                            "lng" => $data['lng'],
                            "created_on" => rand($date, $max_date)
                        ];
                        // $joglo->copyfrom($objek);

                        // SAVE IT HERE
                        return $objek;
                    },
                    \Libs\Koordinate::instance()->generate_n($s, $s['radius'], $n)
                ));

                $date = $max_date;
            }
        }
        $d=0;
        echo "id,lat,lng\n";
        /*\View\Api::success*/(array_map(function ($e) use (&$d) {
            $e['created_on'] = date("Y-m-d H:i:s", $e['created_on']);

            $d++;
            echo "$d," . $e['lat'] . "," . $e['lng'] . "\n";
            return $e;
        }, $datas));
    }
}
